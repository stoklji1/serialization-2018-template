#include <iostream>

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <cstring>
#include <unistd.h>


#include <jsoncpp/json/json.h>
#include <google/protobuf/message.h>

#include "measurements.pb.h"
#include "measurementinfo.h"
#include "dataset.h"
#include "result.h"
#include "cpx.hh"

//#include <boost/array.hpp>
#include <boost/asio.hpp>

using namespace std;
using namespace google::protobuf;
using boost::asio::ip::tcp;

void processJSON(tcp::iostream& stream){
    Json::Value val;
    Json::Reader reader;

    std::vector<Dataset> datasets;
    std::vector<Result> results;

    /* Read json string from the stream */
    string s;
    getline(stream, s, '\0');

    /* Parse string */
    reader.parse(s, val);

    datasets.clear();
    results.clear();
    for (int i = 0; i < val.size(); i++) {
        datasets.emplace_back();
        datasets[i].Deserialize(val[i]);
        /* Calculate averages */
        results.emplace_back(datasets[i].getInfo(), datasets[i].getRecords());
    }

    /* Create output JSON structure */
    Json::Value out;
//    Json::FastWriter writer;
    Json::StyledWriter writer;
    for (int i = 0; i < results.size(); i++) {
        Json::Value result;
        results[i].Serialize(result);
        out[i] = result;
    }

    /* Send the result back */
    std::string output = writer.write(out);
    stream << output;
    cout << output;
}

void processAvro(tcp::iostream& stream){
    char buf[4];
    stream.read(buf, 4);

    //bigendian problem fixing
    char temp = buf[0];
    buf[0] = buf[3];
    buf[3] = temp;
    temp = buf[1];
    buf[1] = buf[2];
    buf[2] = temp;

    int size;
    memcpy(&size, buf, 4);
    char *buffer = new char[size];
    stream.read(buffer, size);

    std::unique_ptr<avro::InputStream> inStream = avro::memoryInputStream((uint8_t*) buffer, size);
    avro::DecoderPtr decoder = avro::binaryDecoder();
    decoder->init(*inStream);

    c::AMeasurements measurements;
    avro::decode(*decoder, measurements);

    for(int i = 0; i < measurements.report.size(); i++){
        c::AAverages *averages = &measurements.report[i].averages;
        double average = 0;
        for(int j = 0; j < averages->DOWNLOAD.size(); j++) {
            average += averages->DOWNLOAD[j];
        }
        average /= averages->DOWNLOAD.size();
        averages->DOWNLOAD.clear();
        averages->DOWNLOAD.push_back(average);

        average = 0;
        for(int j = 0; j < averages->UPLOAD.size(); j++) {
            average += averages->UPLOAD[j];
        }
        average /= averages->UPLOAD.size();
        averages->UPLOAD.clear();
        averages->UPLOAD.push_back(average);

        average = 0;
        for(int j = 0; j < averages->PING.size(); j++) {
            average += averages->PING[j];
        }
        average /= averages->PING.size();
        averages->PING.clear();
        averages->PING.push_back(average);
    }

    std::unique_ptr<avro::OutputStream> outStream = avro::ostreamOutputStream(stream);
    avro::EncoderPtr encoder = avro::binaryEncoder();
    encoder->init(*outStream);

    avro::encode(*encoder, measurements);
    encoder->flush();

    delete [] buffer;
}

void processProtobuf(tcp::iostream& stream){
    char buf[4];
    stream.read(buf, 4);

    //bigendian problem fixing
    char temp = buf[0];
    buf[0] = buf[3];
    buf[3] = temp;
    temp = buf[1];
    buf[1] = buf[2];
    buf[2] = temp;

    int size;
    memcpy(&size, buf, 4);
    char *buffer = new char[size];
    stream.read(buffer, size);
    esw::Measurements measurements;
    measurements.ParseFromArray(buffer, size);
    for(int i = 0; i < measurements.report_size(); i++) {
        esw::PAverages *average = measurements.mutable_report(i)->mutable_averages();
        double datas = 0;
        for(int j = 0; j < average->download_size(); j++){
            datas += average->download(j);
        }
        datas /= average->download_size();
        average->clear_download();
        average->add_download(datas);

        datas = 0;
        for(int j = 0; j < average->upload_size(); j++){
            datas += average->upload(j);
        }
        datas /= average->upload_size();
        average->clear_upload();
        average->add_upload(datas);

        datas = 0;
        for(int j = 0; j < average->ping_size(); j++){
            datas += average->ping(j);
        }
        datas /= average->ping_size();
        average->clear_ping();
        average->add_ping(datas);
    }

    size = measurements.ByteSizeLong();
    delete [] buffer;
    buffer = new char[size];
    measurements.SerializeToArray(buffer, size);
    stream.write(buffer, size);
    delete [] buffer;
}

int main(int argc, char *argv[]) {

    if (argc != 3) {
        cout << "Error: two arguments required - ./server  <port> <protocol>" << endl;
        return 1;
    }



    // unsigned short int port = 12345;
    unsigned short int port = atoi(argv[1]);

    // std::string protocol = "json";
    std::string protocol(argv[2]);
    try {
        boost::asio::io_service io_service;

        tcp::endpoint endpoint(tcp::v4(), port);
        tcp::acceptor acceptor(io_service, endpoint);

        while (true) {
            tcp::iostream stream;
            boost::system::error_code ec;
            acceptor.accept(*stream.rdbuf(), ec);

            if(protocol == "json"){
                processJSON(stream);
            }else if(protocol == "avro"){
                processAvro(stream);
            }else if(protocol == "proto"){
                processProtobuf(stream);
            }else{
                throw std::logic_error("Protocol not yet implemented");
            }

        }

    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}
