package cz.esw.serialization.avro;

import cz.esw.serialization.json.DataType;
import cz.esw.serialization.json.MeasurementInfo;

import java.util.List;
import java.util.Map;

public class ADataset {
    private MeasurementInfo info;
    private Map<DataType, List<Double>> records;

    public ADataset() {
    }

    public MeasurementInfo getInfo() {
        return info;
    }

    public void setInfo(MeasurementInfo info) {
        this.info = info;
    }

    public Map<DataType, List<Double>> getRecords() {
        return records;
    }

    public void setRecords(Map<DataType, List<Double>> records) {
        this.records = records;
    }
}
