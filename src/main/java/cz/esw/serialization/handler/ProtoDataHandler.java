package cz.esw.serialization.handler;

import cz.esw.serialization.ResultConsumer;
import cz.esw.serialization.json.DataType;
import cz.esw.serialization.json.MeasurementInfo;
import cz.esw.serialization.json.Result;
import cz.esw.serialization.proto.*;

import java.io.*;
import java.util.*;

/**
 * @author Marek Cuchý (CVUT)
 */
public class ProtoDataHandler implements DataHandler {

    Measurements.Builder measurements;

    private final InputStream is;
    private final OutputStream os;

    protected Map<Integer, PDataset> datasets;

    public ProtoDataHandler(InputStream is, OutputStream os) {
        this.is = is;
        this.os = os;
    }

    @Override
    public void initialize() {
        datasets = new HashMap<>();
        measurements = Measurements.newBuilder();
    }

    @Override
    public void handleNewDataset(int datasetId, long timestamp, String measurerName) {
        PDataset dataset = new PDataset();
        dataset.setInfo(new MeasurementInfo(datasetId, timestamp, measurerName));
        dataset.setRecords(new EnumMap<>(DataType.class));
        datasets.put(datasetId, dataset);
    }

    @Override
    public void handleValue(int datasetId, DataType type, double value) {
        PDataset dataset = datasets.get(datasetId);
        if (dataset == null) {
            throw new IllegalArgumentException("Dataset with id " + datasetId + " not initialized.");
        }
        dataset.getRecords().computeIfAbsent(type, t -> new ArrayList<>()).add(value);
    }

    @Override
    public void getResults(ResultConsumer consumer) throws IOException {
        setMeasurements();
        Measurements builtMeasurements = measurements.build();

        int messageSize = builtMeasurements.getSerializedSize();

        DataOutputStream dos = new DataOutputStream(os);
        dos.writeInt(messageSize);
        dos.flush();

        builtMeasurements.writeTo(os);

        Measurements incomingMessage = Measurements.parseFrom(is);

        Result[] results = parseMeasurement(incomingMessage);

        for (Result result : results) {
            MeasurementInfo info = result.getInfo();
            consumer.acceptMeasurementInfo(info.getId(), info.getTimestamp(), info.getMeasurerName());
            result.getAverages().forEach(consumer::acceptResult);
        }

        os.close();
        is.close();
    }

    private void setMeasurements() {
        for (Map.Entry<Integer, PDataset> entry : datasets.entrySet()) {
            PMeasurementReport.Builder report = PMeasurementReport.newBuilder();
            report.setAverages(setAverage(entry.getValue().getRecords()));
            report.setMeasurementInfo(setMeasurementInfo(entry.getValue().getInfo()));
            measurements.addReport(report.build());
        }
    }

    private PAverages setAverage(Map<DataType, List<Double>> records) {
        PAverages.Builder pAverages = PAverages.newBuilder();
        for (Map.Entry<DataType, List<Double>> entry : records.entrySet()) {
            switch (entry.getKey()) {
                case DOWNLOAD -> pAverages.addAllDOWNLOAD(entry.getValue());
                case PING -> pAverages.addAllPING(entry.getValue());
                case UPLOAD -> pAverages.addAllUPLOAD(entry.getValue());
            }
        }
        return pAverages.build();
    }

    private PMeasurementInfo setMeasurementInfo(MeasurementInfo info) {
        PMeasurementInfo.Builder pMeasurementInfo = PMeasurementInfo.newBuilder();
        pMeasurementInfo.setId(info.getId());
        pMeasurementInfo.setMeasurerName(info.getMeasurerName());
        pMeasurementInfo.setTimestamp(info.getTimestamp());
        return pMeasurementInfo.build();
    }

    private Result[] parseMeasurement(Measurements measurements) {
        List<Result> resultList = new ArrayList<>();
        for (PMeasurementReport measurementReport : measurements.getReportList()) {
            Result result = new Result();
            PMeasurementInfo measurementInfo = measurementReport.getMeasurementInfo();
            result.setInfo(new MeasurementInfo(measurementInfo.getId(), measurementInfo.getTimestamp(), measurementInfo.getMeasurerName()));
            result.setAverages(setAveragesMap(measurementReport.getAverages()));
            resultList.add(result);
        }
        return resultList.toArray(Result[]::new);
    }

    private Map<DataType, Double> setAveragesMap(PAverages averages) {
        Map<DataType, Double> result = new HashMap<>();
        result.put(DataType.DOWNLOAD, averages.getDOWNLOAD(0));
        result.put(DataType.UPLOAD, averages.getUPLOAD(0));
        result.put(DataType.PING, averages.getPING(0));
        return result;
    }
}
