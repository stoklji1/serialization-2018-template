package cz.esw.serialization.handler;

import cz.esw.serialization.ResultConsumer;
import cz.esw.serialization.avro.*;
import cz.esw.serialization.json.DataType;
import cz.esw.serialization.json.MeasurementInfo;
import cz.esw.serialization.json.Result;
import org.apache.avro.io.*;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;

import java.io.*;
import java.util.*;

/**
 * @author Marek Cuchý (CVUT)
 */
public class AvroDataHandler implements DataHandler {

    AMeasurements.Builder measurements;
    private final InputStream is;
    private final OutputStream os;

    protected Map<Integer, ADataset> datasets;

    public AvroDataHandler(InputStream is, OutputStream os) {
        this.is = is;
        this.os = os;
    }

    @Override
    public void initialize() {
        datasets = new HashMap<>();
        measurements = AMeasurements.newBuilder();
    }

    @Override
    public void handleNewDataset(int datasetId, long timestamp, String measurerName) {
        ADataset dataset = new ADataset();
        dataset.setInfo(new MeasurementInfo(datasetId, timestamp, measurerName));
        dataset.setRecords(new EnumMap<>(DataType.class));
        datasets.put(datasetId, dataset);
    }

    @Override
    public void handleValue(int datasetId, DataType type, double value) {
        ADataset dataset = datasets.get(datasetId);
        if (dataset == null) {
            throw new IllegalArgumentException("Dataset with id " + datasetId + " not initialized.");
        }
        dataset.getRecords().computeIfAbsent(type, t -> new ArrayList<>()).add(value);
    }

    @Override
    public void getResults(ResultConsumer consumer) throws IOException {
        setMeasurements();
        AMeasurements builtMeasurements = measurements.build();

        DatumWriter<AMeasurements> datumWriter = new SpecificDatumWriter<>(AMeasurements.class);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(byteArrayOutputStream, null);
        datumWriter.write(builtMeasurements, encoder);
        encoder.flush();

        int messageSize = byteArrayOutputStream.size();

        DataOutputStream dos = new DataOutputStream(os);
        dos.writeInt(messageSize);
        dos.flush();

        os.write(byteArrayOutputStream.toByteArray());
        os.flush();

        AMeasurements incomingMessage = new AMeasurements();
        DatumReader<AMeasurements> datumReader = new SpecificDatumReader<>(AMeasurements.class);
        DecoderFactory decoderFactory = new DecoderFactory();
        BinaryDecoder binaryDecoder = decoderFactory.binaryDecoder(is, null);
        datumReader.read(incomingMessage, binaryDecoder);

        Result[] results = readMeasure(incomingMessage);

        for (Result result : results) {
            MeasurementInfo info = result.getInfo();
            consumer.acceptMeasurementInfo(info.getId(), info.getTimestamp(), info.getMeasurerName());
            result.getAverages().forEach(consumer::acceptResult);
        }
        os.close();
        is.close();
    }

    private void setMeasurements() {
        List<AMeasurementReport> reports = new ArrayList<>();
        for (Map.Entry<Integer, ADataset> entry : datasets.entrySet()) {
            ADataset value = entry.getValue();
            AMeasurementReport.Builder report = AMeasurementReport.newBuilder();
            report.setAverages(setAverage(value.getRecords()));
            report.setMeasurementInfo(setMeasurementInfo(value.getInfo()));
            reports.add(report.build());
        }
        measurements.setReport(reports);
    }

    private AAverages setAverage(Map<DataType, List<Double>> records) {
        AAverages.Builder aAverages = AAverages.newBuilder();
        for (Map.Entry<DataType, List<Double>> entry : records.entrySet()) {
            switch (entry.getKey()) {
                case DOWNLOAD -> aAverages.setDOWNLOAD(entry.getValue());
                case PING -> aAverages.setPING(entry.getValue());
                case UPLOAD -> aAverages.setUPLOAD(entry.getValue());
            }
        }
        return aAverages.build();
    }

    private AMeasurementInfo setMeasurementInfo(MeasurementInfo info) {
        AMeasurementInfo.Builder aMeasurementInfo = AMeasurementInfo.newBuilder();
        aMeasurementInfo.setId(info.getId());
        aMeasurementInfo.setMeasurerName(info.getMeasurerName());
        aMeasurementInfo.setTimestamp(info.getTimestamp());
        return aMeasurementInfo.build();
    }

    private Result[] readMeasure(AMeasurements incomingMeasurements) {
        List<Result> resultList = new ArrayList<>();
        for (AMeasurementReport measurementReport : incomingMeasurements.getReport()) {
            Result result = new Result();
            AMeasurementInfo measurementInfo = measurementReport.getMeasurementInfo();
            result.setInfo(new MeasurementInfo(measurementInfo.getId(), measurementInfo.getTimestamp(), measurementInfo.getMeasurerName()));
            result.setAverages(setAveragesMap(measurementReport.getAverages()));
            resultList.add(result);
        }
        return resultList.toArray(Result[]::new);
    }

    private Map<DataType, Double> setAveragesMap(AAverages averages) {
        Map<DataType, Double> result = new HashMap<>();
        result.put(DataType.DOWNLOAD, averages.getDOWNLOAD().get(0));
        result.put(DataType.UPLOAD, averages.getUPLOAD().get(0));
        result.put(DataType.PING, averages.getPING().get(0));
        return result;
    }
}
